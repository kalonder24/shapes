//
//  main.swift
//  MasterClassAssignment
//
//  Created by Kaloyan on 27.06.22.
//

import Foundation


class InputManager {
    private static var inputs: [Inputs] = [StdInInput(), FileInput(), RandomInput()]
    
    static func generateFigures() {
        print("Enter type of generating figures: ")
        let type = readLine()!
        for input in inputs {
            if input.isProperInput(input: type) {
                let shapes = input.generateFigures()
                for shape in shapes {
                    print(shape.toString())
                }
            }
        }
    }
}

protocol Inputs {
    func isProperInput(input: String) -> Bool
    func generateFigures() -> [Shape]
}

extension Inputs {
    var figureProvider: FigureProvider {
        let figureProvider = FigureProvider()
        return figureProvider
    }
}

class StdInInput: Inputs {
    func isProperInput(input: String) -> Bool {
        return input == "stdin"
    }
    
    func generateFigures() -> [Shape] {
        print("Enter the capacity of the shape array:")
        let capacity = Int(readLine()!)!
        var figuresArray = Array<Shape> ()
        for _ in 0..<capacity {
            print("Enter shape description:")
            let shapeDescription = readLine()!
            if let shape = self.figureProvider.createFigureFromString(shapeRepresentation: shapeDescription) {
                figuresArray.append(shape)
            }
        }
        return figuresArray
    }
}

class RandomInput: Inputs {
    func isProperInput(input: String) -> Bool {
        return input == "random"
    }
    
    func generateFigures() -> [Shape] {
        print("Enter the capacity of the shape array:")
        let capacity = Int(readLine()!)!
        var figuresArray = Array<Shape> ()
        //figuresArray.reserveCapacity(capacity)
        for _ in 0..<capacity {
            if let shape = self.figureProvider.createFigureFromString(shapeRepresentation: generateInputStringForShape()) {
                figuresArray.append(shape)
            }
        }
        return figuresArray
    }
    
    private func generateInputStringForShape() -> String {
        var shapeStringRepresentation: String = ""
        let shapes = ["circle" : 1, "rectangle" : 2, "triangle" : 3]
        let randomShape = shapes.randomElement()!
        shapeStringRepresentation.append(randomShape.key)
        for _ in 1...randomShape.value {
            shapeStringRepresentation.append(" " + String(Int.random(in: 0...100)))
        }
        return shapeStringRepresentation
    }
}

class FileInput: Inputs {
    func isProperInput(input: String) -> Bool {
        return input == "file"
    }
    
    func generateFigures() -> [Shape] {
        print("Enter the name of the file: ")
        let fileName = readLine()
        var lines = [String]()
        var figuresArray = Array<Shape> ()
        if let path = Bundle.main.path(forResource: fileName, ofType: "txt"){
            do {
                let data = try String(contentsOfFile: path, encoding: .utf8)
                lines = data.components(separatedBy: .newlines)
            } catch {
                print(error)
            }
        }
        if let capacity = Int(lines[0]) {
            for i in 1..<capacity {
                if let shape = self.figureProvider.createFigureFromString(shapeRepresentation: lines[i]) {
                    figuresArray.append(shape)
                }
            }
        } else {
            print("Error!")
            return figuresArray
        }
        return figuresArray
    }
    
    
}


class FigureProvider {
    private var factories: [FigureCreator] = [TriangleCreator(),CircleCreator(),RectangleCreator()]
    
    func createFigureFromString(shapeRepresentation: String) -> Shape? {
        for factory in factories {
            if let shape = factory.createFigureFromString(input: shapeRepresentation) {
                return shape
            }
        }
        return nil
    }
}


protocol FigureCreator {
    func createFigureFromString(input: String) -> Shape?
}

class TriangleCreator: FigureCreator {
    func createFigureFromString(input: String) -> Shape? {
        guard isTriangle(input: input) else {
            return nil
        }
        let sideA = Double(input.split(separator: " ")[1])!
        let sideB = Double(input.split(separator: " ")[2])!
        let sideC = Double(input.split(separator: " ")[3])!
        return Triangle(sideA: sideA, sideB: sideB, sideC: sideC)
    }
    
    private func isTriangle(input: String) -> Bool {
        let subStringsOfInput = input.split(separator: " ")
        guard subStringsOfInput.count == 4 else {
            return false
        }
        guard String(subStringsOfInput[0]) == "triangle" else {
            return false
        }
        guard Double(subStringsOfInput[1]) != nil else {
            return false
        }
        guard Double(subStringsOfInput[2]) != nil else {
            return false
        }
        guard Double(subStringsOfInput[3]) != nil else {
            return false
        }
        return true
    }
}

class RectangleCreator: FigureCreator {
    func createFigureFromString(input: String) -> Shape? {
        guard isRectangle(input: input) else {
            return nil
        }
        let sideA = Double(input.split(separator: " ")[1])!
        let sideB = Double(input.split(separator: " ")[2])!
        return Rectangle(sideA: sideA, sideB: sideB)
    }
    
    private func isRectangle(input: String) -> Bool {
        let subStringsOfInput = input.split(separator: " ")
        guard subStringsOfInput.count == 3 else {
            return false
        }
        guard String(subStringsOfInput[0]) == "rectangle" else {
            return false
        }
        guard Double(subStringsOfInput[1]) != nil else {
            return false
        }
        guard Double(subStringsOfInput[2]) != nil else {
            return false
        }
        return true
    }
}

class CircleCreator: FigureCreator {
    func createFigureFromString(input: String) -> Shape? {
        guard isCircle(input: input) else {
            return nil
        }
        let radius = Double(input.split(separator: " ")[1])!
        return Circle(radius: radius)
    }
    
    private func isCircle(input: String) -> Bool {
        let subStringsOfInput = input.split(separator: " ")
        guard subStringsOfInput.count == 2 else {
            return false
        }
        guard String(subStringsOfInput[0]) == "circle" else {
            return false
        }
        guard Double(subStringsOfInput[1]) != nil else {
            return false
        }
        return true
    }
}



protocol Shape: NSCopying {
    
    func perimeter() -> Double
    func toString() -> String
    
}

class Triangle: Shape {
    
    private var a: Double
    private var b: Double
    private var c: Double
    
    func perimeter() -> Double {
        return a + b + c
    }
    
    func toString() -> String {
        return "triangle " + String(a) + " " + String(b) + " " + String(c)
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return Triangle(sideA: a, sideB: b, sideC: c)
    }
    
    
    init(sideA: Double, sideB: Double, sideC: Double) {
        self.a = sideA
        self.b = sideB
        self.c = sideC
    }
}

class Circle: Shape {
    
    private var radius: Double
    
    func perimeter() -> Double {
        return 2 * radius * CGFloat.pi
    }
    
    func toString() -> String {
        return "circle " + String(radius)
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return Circle(radius: radius)
    }
    
    
    init(radius: Double) {
        self.radius = radius
    }
}

class Rectangle: Shape {
    
    
    private var a: Double
    private var b: Double
    
    func perimeter() -> Double {
        return 2 * (a + b)
    }
    
    func toString() -> String {
        return "rectangle " + String(a) + " " + String(b)
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return Rectangle(sideA: a, sideB: b)
    }
    
    init(sideA: Double, sideB: Double) {
        self.a = sideA
        self.b = sideB
    }
}

InputManager.generateFigures()







